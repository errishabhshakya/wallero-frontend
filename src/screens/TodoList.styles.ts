import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  title: {
    fontSize: 28,
    textTransform: 'uppercase',
    alignSelf: 'center',
    marginTop: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    fontSize: 20,
    borderColor: 'black',
    borderWidth: 1,
    paddingVertical: 5,
    marginVertical: 10,
    paddingHorizontal: 5,
  },
  todoItem: {
    fontSize: 20,
    flex: 2,
  },
  update: {
    color: 'blue',
    fontWeight: '600',
    marginLeft: 10,
  },
  delete: {
    color: 'red',
    fontWeight: '700',
    marginLeft: 10,
  },
});
