import {useMutation, useQuery, useQueryClient} from '@tanstack/react-query';
import React, {useEffect, useRef, useState} from 'react';
import {
  Button,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Todo, TodoService} from '../services/todo-service';
import LoadingOverlay from '../components/LoadingOverlay';
import {styles} from './TodoList.styles';

export const TodoList = () => {
  const [todo, setTodo] = useState<string>('');
  const [isUpdatingTodo, setUpdatingTodo] = useState<boolean>(false);

  const textInputRef = useRef<TextInput>(null);

  const selectedTodoId = useRef<number>(-1);

  // Access the client
  const queryClient = useQueryClient();

  // Queries
  const {isPending, data, error} = useQuery<Todo[], Error>({
    queryKey: ['todos'],
    queryFn: TodoService.getTodos,
  });

  // Mutations
  const {
    isPending: isCreateTodoPending,
    mutate: createTodoMutate,
    isSuccess: isCreateTodoSuccess,
    status: createTodoStatus,
  } = useMutation({
    mutationFn: TodoService.createTodo,
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({queryKey: ['todos']});
    },
  });

  const {
    isPending: isUpdateTodoPending,
    mutate: updateTodoMutate,
    isSuccess: isUpdateTodoSuccess,
    status: updateTodoStatus,
  } = useMutation({
    mutationFn: TodoService.updateTodo,
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({queryKey: ['todos']});
    },
  });

  const {isPending: isDeleteTodoPending, mutate: deleteTodoMutate} =
    useMutation({
      mutationFn: TodoService.deleteTodo,
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries({queryKey: ['todos']});
      },
    });

  useEffect(() => {
    if (isCreateTodoSuccess || isUpdateTodoSuccess) {
      textInputRef.current?.clear();
      isUpdateTodoSuccess && setUpdatingTodo(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createTodoStatus, updateTodoStatus]);

  const handleTodoChange = (text: string) => {
    setTodo(text);
  };

  const handleAddTodoPressed = () => {
    createTodoMutate({
      todo: todo,
    });
  };

  const handleUpdateTodoPressed = () => {
    updateTodoMutate({id: selectedTodoId.current, todo: todo});
  };

  const handleCancelPressed = () => {
    setUpdatingTodo(false);
    textInputRef.current?.clear();
  };

  const handleTodoUpdate = (todoItem: Todo) => {
    setUpdatingTodo(true);
    selectedTodoId.current = todoItem.id;
    textInputRef.current?.setNativeProps({text: todoItem.todo});
  };

  const handleTodoDelete = (todoId: number) => {
    deleteTodoMutate(todoId);
  };

  const renderTodoItem = ({item}: {item: Todo}) => {
    return (
      // eslint-disable-next-line react-native/no-inline-styles
      <View key={item.id} style={[styles.row, {marginTop: 10}]}>
        <Text style={styles.todoItem}>{item.todo}</Text>
        <TouchableOpacity onPress={() => handleTodoUpdate(item)}>
          <Text style={styles.update}>Update</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleTodoDelete(item.id)}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const isLoading =
    isPending ||
    isCreateTodoPending ||
    isUpdateTodoPending ||
    isDeleteTodoPending;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Todo App</Text>
      <View>
        <View>
          <TextInput
            ref={textInputRef}
            style={styles.textInput}
            onChangeText={handleTodoChange}
            placeholder="Enter Todo here"
          />
          {isUpdatingTodo ? (
            // eslint-disable-next-line react-native/no-inline-styles
            <View style={[styles.row, {alignSelf: 'center'}]}>
              <Button title="Update Todo" onPress={handleUpdateTodoPressed} />
              <Button title="Cancel" onPress={handleCancelPressed} />
            </View>
          ) : (
            <Button title="Add Todo" onPress={handleAddTodoPressed} />
          )}
        </View>
        {data ? (
          <FlatList
            data={
              // NOTE: Since we are refetching the data after updating Todo, api is changing the order.
              data.sort(
                (a, b) =>
                  new Date(a.createdOn).getTime() -
                  new Date(b.createdOn).getTime(),
              ) ?? []
            }
            renderItem={renderTodoItem}
          />
        ) : (
          <Text>{error?.message}</Text>
        )}
      </View>
      <LoadingOverlay visible={isLoading} />
    </View>
  );
};
