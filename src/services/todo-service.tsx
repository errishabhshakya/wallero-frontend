import {apiInstance} from './api';

export interface Todo {
  id: number;
  todo: string;
  createdOn: string;
  updatedOn: string;
}

interface CreateTodoRequest {
  todo: string;
}

interface UpdateTodoRequest {
  id: number;
  todo: string;
}

export class TodoService {
  static getTodos = async (): Promise<Todo[]> => {
    const {data} = await apiInstance.get('/todos');
    return data;
  };

  static createTodo = async (todo: CreateTodoRequest) => {
    await apiInstance.post('/todo/create', todo);
  };

  static deleteTodo = async (todoId: number) => {
    await apiInstance.delete(`/todo/delete/${todoId}`);
  };

  static updateTodo = async (todo: UpdateTodoRequest) => {
    await apiInstance.put(`/todo/update/${todo.id}`, todo);
  };
}
